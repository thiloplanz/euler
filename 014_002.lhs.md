014 - Längste Collatzfolge
------------------------------

Eine Folge von natürlichen Zahlen ist gegeben durch

* wenn `n` gerade ist, folgt `n/2`
* wenn `n` ungerade ist, folgt `3*n + 1`

Beginnend mit 13 ergibt sich eine Folge mit zehn Elementen, bis `1` erreicht wird:

`13 40 20 10 5 16 8 4 2 1`

Es wird vermutet, daß alle Folgen bei 1 ankommen, aber dies ist noch unbewiesen
("Collatz-Vermutung").

Welche der Zahlen bis einschließlich `N` erzeugt die längste Collatzfolge 
(bei mehreren gleichlangen Folgen soll die größte Startzahl ausgegeben werden)?

Beachte, daß nur die Startzahl durch `N` beschränkt ist, die nachfolgenden Elemente
jedoch größer werden können.



### Vorbereitungen

~~~
``` {.haskell .literate}
import Control.Applicative
import Control.Monad
import System.IO
import Data.List
import Data.Array.IO.Safe

zahl :: IO Int
zahl = read <$> getLine

zahlen :: Int -> IO [Int]
zahlen n = forM [1..n] $ \_ -> zahl

main :: IO ()
main = do
    xs <- zahl >>= zahlen >>= ergebnisse
    forM_ xs $ print

ergebnisse :: [Int] -> IO [Int]
```
~~~

### Zweiter Versuch

Wir füllen schrittweise ein beschreibbares Feld (`IO (IOUArray Int Int)`) mit
den Längen der jeweiligen Collatzfolgen. Jeder Schritt startet bei einer Zahl,
und sobald wir eine Zahl erreicht haben, die bereits im Feld vorkommt, können
wir aufhören.

~~~
``` {.haskell .literate}

type Collatzfeld = IOUArray Int Int

neuesFeld :: Int -> IO Collatzfeld
neuesFeld schranke = newArray (1,schranke) 0

collatz n
    | even n     =   div n 2
    | otherwise  =   3 * n + 1

-- berechne die Collatzfolge beginnend bei `n` bis ein bekannte Zahl auftaucht
-- der Rueckgabewert enthaelt die Elemente und ihre Laengen
collatzFolge :: Collatzfeld -> Int -> IO [(Int, Int)]
collatzFolge feld 1 = return [(1,1)]
collatzFolge feld n = do
   schranke <- snd <$> getBounds feld 
   schonDa <- if (n <= schranke) then (readArray feld n) else return 0
   if schonDa > 0 then
      return [(n, schonDa)]
   else do
      rest <- collatzFolge feld $ collatz n
      return $ (n, 1 + (snd.head $ rest) ) : rest

-- berechne die Folge einer Zahl und aktualisiere das Feld entsprechend
schritt :: Collatzfeld -> Int -> IO Collatzfeld
schritt feld n = do
   schonDa <- readArray feld n
   if schonDa > 0 then 
      return feld
   else do
      schranke <- snd <$> getBounds feld 
      folge <- filter ((<=schranke).fst) <$> (collatzFolge feld n)
      forM_ folge $ do \(c, l) -> writeArray feld c l
      return feld


ergebnisse xs = do
      feld <- neuesFeld $ 2 * schranke   -- etwas Luft nach oben
       -- maxima berechnen
      maxima <- neuesFeld schranke
      forM_ [1..schranke] $ \i -> do
         schritt feld i
         vorher <- if i > 1 then readArray maxima (i-1) else return 1
         vorherHat <- readArray feld vorher
         jetzt <- readArray feld i
         writeArray maxima i $ if vorherHat > jetzt then vorher else i 
      forM xs $ readArray maxima
   where
       schranke = maximum xs
```
~~~

